#include "JukeBox.h"
#include <windows.h>
#include <mmsystem.h>
#include <algorithm>

JukeBox::JukeBox()
{
}

JukeBox::JukeBox(vector<Song> v)
{
	for (int i = 0; i < v.size(); i++) { // init the playlist
		musicList.push_back(v[i]);
	}

	std::random_shuffle(musicList.begin(), musicList.end());

}

bool JukeBox::StartSong()
{
	//start playing the song list
	while (true)
	{
		string path = musicList[currentSongIndex].GetPath();
		string cmdOpen = "open \"" + path + "\" type mpegvideo alias myaudio";
		LPCSTR cfmd = cmdOpen.c_str();
		mciSendString(cfmd, NULL, 0, NULL);
		string cmd = ("play \"myaudio\" from " + musicList[currentSongIndex].start + " to " + musicList[currentSongIndex].end + " wait");
		LPCSTR cmmd = cmd.c_str();

		mciSendString(cmmd, NULL, 0, NULL);
		mciSendString("close myaudio", NULL, 0, NULL);
		currentSongIndex = (currentSongIndex + 1) % musicList.size();
	}

	return EXIT_SUCCESS;
}
music_genre JukeBox::GetCurrentSongGenre() 
{
	return musicList[currentSongIndex].GetMusicGenre(); //return the current music genre.
}

bool JukeBox::StopSong()
{
	PlaySound(NULL, NULL, 0);
	return EXIT_SUCCESS;
}





