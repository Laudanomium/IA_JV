#pragma once
#ifndef JUKEBOX_H
#define JUKEBGOX_H


#include <string>
#include <vector>
#include <iostream>
#include "MusicalGenre.h"
#include "Song.h"

using namespace std;

class JukeBox 
{
public:
	JukeBox();
	JukeBox(vector<Song>);
	bool StartSong();
	bool NextSong();
	bool StopSong();
	music_genre GetCurrentSongGenre();

private:
	vector<Song> musicList;
	int currentSongIndex = 0;

};

#endif
