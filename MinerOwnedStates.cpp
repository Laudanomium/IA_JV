﻿#include "MinerOwnedStates.h"
#include "../../Common/fsm/State.h"
#include "Miner.h"
#include "Locations.h"
#include "../../Common/messaging/Telegram.h"
#include "MessageDispatcher.h"
#include "MessageTypes.h"
#include "../../Common/Time/CrudeTimer.h"
#include "EntityNames.h"
#include "Drunkard.h"
#include "EntityManager.h"
#include <iostream>
using std::cout;


#ifdef TEXTOUTPUT
#include <fstream>
extern std::ofstream os;
#define cout os
#endif
#include "../../Common/misc/utils.h"


//------------------------------------------------------------------------methods for EnterMineAndDigForNugget
EnterMineAndDigForNugget* EnterMineAndDigForNugget::Instance()
{
  static EnterMineAndDigForNugget instance;

  return &instance;
}


void EnterMineAndDigForNugget::Enter(Miner* pMiner)
{
  //if the miner is not already located at the goldmine, he must
  //change location to the gold mine
  if (pMiner->Location() != goldmine)
  {
    cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Walkin' to the goldmine";

    pMiner->ChangeLocation(goldmine);
  }
}


void EnterMineAndDigForNugget::Execute(Miner* pMiner)
{  
  //Now the miner is at the goldmine he digs for gold until he
  //is carrying in excess of MaxNuggets. If he gets thirsty during
  //his digging he packs up work for a while and changes state to
  //gp to the saloon for a whiskey.
  pMiner->AddToGoldCarried(1);

  pMiner->IncreaseFatigue();

  cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Pickin' up a nugget";

  //if enough gold mined, go and put it in the bank
  if (pMiner->PocketsFull())
  {
    pMiner->GetFSM()->ChangeState(VisitBankAndDepositGold::Instance());
  }

  if (pMiner->Thirsty())
  {
    pMiner->GetFSM()->ChangeState(QuenchThirst::Instance());
  }
}


void EnterMineAndDigForNugget::Exit(Miner* pMiner)
{
  cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " 
       << "Ah'm leavin' the goldmine with mah pockets full o' sweet gold";
}


bool EnterMineAndDigForNugget::OnMessage(Miner* pMiner, const Telegram& msg)
{
  //send msg to global message handler
  return false;
}

//------------------------------------------------------------------------methods for VisitBankAndDepositGold

VisitBankAndDepositGold* VisitBankAndDepositGold::Instance()
{
  static VisitBankAndDepositGold instance;

  return &instance;
}

void VisitBankAndDepositGold::Enter(Miner* pMiner)
{  
  //on entry the miner makes sure he is located at the bank
  if (pMiner->Location() != bank)
  {
    cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Goin' to the bank. Yes siree";

    pMiner->ChangeLocation(bank);
  }
}


void VisitBankAndDepositGold::Execute(Miner* pMiner)
{
  //deposit the gold
  pMiner->AddToWealth(pMiner->GoldCarried());
    
  pMiner->SetGoldCarried(0);

  cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " 
       << "Depositing gold. Total savings now: "<< pMiner->Wealth();

  //wealthy enough to have a well earned rest?
  if (pMiner->Wealth() >= ComfortLevel)
  {
    cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " 
         << "WooHoo! Rich enough for now. Back home to mah li'lle lady";
      
    pMiner->GetFSM()->ChangeState(GoHomeAndSleepTilRested::Instance());      
  }

  //otherwise get more gold
  else 
  {
    pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
  }
}


void VisitBankAndDepositGold::Exit(Miner* pMiner)
{
  cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Leavin' the bank";
}


bool VisitBankAndDepositGold::OnMessage(Miner* pMiner, const Telegram& msg)
{
  //send msg to global message handler
  return false;
}
//------------------------------------------------------------------------methods for GoHomeAndSleepTilRested

GoHomeAndSleepTilRested* GoHomeAndSleepTilRested::Instance()
{
  static GoHomeAndSleepTilRested instance;

  return &instance;
}

void GoHomeAndSleepTilRested::Enter(Miner* pMiner)
{
  if (pMiner->Location() != shack)
  {
    cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Walkin' home";

    pMiner->ChangeLocation(shack); 

    //let the wife know I'm home
    Dispatch->DispatchMessage(SEND_MSG_IMMEDIATELY, //time delay
                              pMiner->ID(),        //ID of sender
                              ent_Elsa,            //ID of recipient
                              Msg_HiHoneyImHome,   //the message
                              NO_ADDITIONAL_INFO);    
  }
}

void GoHomeAndSleepTilRested::Execute(Miner* pMiner)
{ 
  //if miner is not fatigued start to dig for nuggets again.
  if (!pMiner->Fatigued())
  {
     cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " 
          << "All mah fatigue has drained away. Time to find more gold!";

     pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
  }

  else 
  {
    //sleep
    pMiner->DecreaseFatigue();

    cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "ZZZZ... ";
  } 
}

void GoHomeAndSleepTilRested::Exit(Miner* pMiner)
{ 
}


bool GoHomeAndSleepTilRested::OnMessage(Miner* pMiner, const Telegram& msg)
{
   SetTextColor(BACKGROUND_RED|FOREGROUND_RED|FOREGROUND_GREEN|FOREGROUND_BLUE);

   switch(msg.Msg)
   {
   case Msg_StewReady:

     cout << "\nMessage handled by " << GetNameOfEntity(pMiner->ID()) 
     << " at time: " << Clock->GetCurrentTime();

     SetTextColor(FOREGROUND_RED|FOREGROUND_INTENSITY);

     cout << "\n" << GetNameOfEntity(pMiner->ID()) 
          << ": Okay Hun, ahm a comin'!";

     pMiner->GetFSM()->ChangeState(EatStew::Instance());
      
     return true;

   }//end switch

   return false; //send message to global message handler
}

//------------------------------------------------------------------------QuenchThirst

QuenchThirst* QuenchThirst::Instance()
{
  static QuenchThirst instance;

  return &instance;
}

void QuenchThirst::Enter(Miner* pMiner)
{
  if (pMiner->Location() != saloon)
  {    
    pMiner->ChangeLocation(saloon);
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"Boy, ah sure is thusty! Walking to the saloon";

	Drunkard* d = (Drunkard*) EntityMgr->GetEntityFromID(ent_Bobby);
	if (d->Location() == saloon)
	{
		Dispatch->DispatchMessage(
			SEND_MSG_IMMEDIATELY,
			pMiner->ID(),
			ent_Bobby,
			Msg_BobIsHere,
			NO_ADDITIONAL_INFO);
		pMiner->GetFSM()->ChangeState(TalkWithBobby::Instance());
	}
  }
}

void QuenchThirst::Execute(Miner* pMiner)
{
	if (pMiner->GoldCarried() > 0) 
	{
		pMiner->BuyAndDrinkAWhiskey();
		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
			"That's mighty fine sippin' liquer";
	}

	// If the drunkard is in the saloon, Bob tells him that he is here too
	Drunkard* d = (Drunkard*)EntityMgr->GetEntityFromID(ent_Bobby);
	if (d->Location() == saloon)
	{
		Dispatch->DispatchMessage(
			SEND_MSG_IMMEDIATELY,
			pMiner->ID(),
			ent_Bobby,
			Msg_BobIsHere,
			NO_ADDITIONAL_INFO);
	}
	pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
}


void QuenchThirst::Exit(Miner* pMiner)
{

}


bool QuenchThirst::OnMessage(Miner* pMiner, const Telegram& msg)
{
	SetTextColor(BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	//If Bobby comes in the saloon, Bob talks with him
	switch (msg.Msg)
	{
	case Msg_BobbyIsHere:

		cout << "\nMessage handled by " << GetNameOfEntity(pMiner->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(FOREGROUND_RED | FOREGROUND_INTENSITY);
		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": "
			<< "Bobby 's in the place !";
		pMiner->GetFSM()->ChangeState(TalkWithBobby::Instance());

		return true;
	}

	return false;
}

//------------------------------------------------------------------------EatStew

EatStew* EatStew::Instance()
{
  static EatStew instance;

  return &instance;
}


void EatStew::Enter(Miner* pMiner)
{
  cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Smells Reaaal goood Elsa!";
}

void EatStew::Execute(Miner* pMiner)
{
  cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Tastes real good too!";

  pMiner->GetFSM()->RevertToPreviousState();
}

void EatStew::Exit(Miner* pMiner)
{ 
  cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " << "Thankya li'lle lady. Ah better get back to whatever ah wuz doin'";
}


bool EatStew::OnMessage(Miner* pMiner, const Telegram& msg)
{
  //send msg to global message handler
  return false;
}



GoToBrothel* GoToBrothel::Instance()
{
	static GoToBrothel instance;
	return &instance;
}

void GoToBrothel::Enter(Miner* pMiner)
{
	// If Elsa doesn't want Bob to come home, Bob goes to the brothel instead
	if (pMiner->Location() != brothel)
	{
		pMiner->ChangeLocation(brothel);
		cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
			"Gettin' laid tonight ! On my way to the brothel";
	}
}

void GoToBrothel::Execute(Miner* pMiner)
{
	// Bob 'sleeps' at the brothel until he is ready to dig for some gold
	pMiner->DecreaseFatigue();
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"Feelin' good";

	if (!pMiner->Fatigued())
	{
		pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
	}
}

void GoToBrothel::Exit(Miner* pMiner)
{
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"What a time to be alive! I'm totally rested, let's go back to the mine";
}

bool GoToBrothel::OnMessage(Miner* agent, const Telegram& msg)
{
	return false;
}



SendSextoToElsa* SendSextoToElsa::Instance()
{
	static SendSextoToElsa instance;
	return &instance;
}

void SendSextoToElsa::Enter(Miner* pMiner)
{
	if (pMiner->Location() != street)
	{
		pMiner->ChangeLocation(street);
	}

	// Due to his mood, Bob wants to go back home to spend some time with his
	// wife
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"Gettin' hot in here! I'm going outside";

	Dispatch->DispatchMessage(
		SEND_MSG_IMMEDIATELY,
		pMiner->ID(),
		ent_Elsa,
		Msg_IWantYou,
		NO_ADDITIONAL_INFO
	);
	SetTextColor(FOREGROUND_RED | FOREGROUND_INTENSITY);
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"Let's have some fun tonight !";

	// Bob warns the drunkard that he is leaving the saloon
	Dispatch->DispatchMessage(
		SEND_MSG_IMMEDIATELY,
		pMiner->ID(),
		ent_Bobby,
		Msg_LeavingSaloon,
		NO_ADDITIONAL_INFO
	);

}

void SendSextoToElsa::Execute(Miner* pMiner)
{
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"I miss you Elsa, I want to hug you real' good tonight!";
}

void SendSextoToElsa::Exit(Miner* pMiner)
{
}

bool SendSextoToElsa::OnMessage(Miner* pMiner, const Telegram& msg)
{
	// Depending on Elsa's mood, Bob goes back home or spends the night
	// in the closest brothel
	SetTextColor(BACKGROUND_RED | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	switch (msg.Msg)
	{
	case Msg_DontComeHome:
		cout << "\nMessage handled by " << GetNameOfEntity(pMiner->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(FOREGROUND_RED | FOREGROUND_INTENSITY);
		pMiner->GetFSM()->ChangeState(GoToBrothel::Instance());
		return true;

	case Msg_ComeHome:
		cout << "\nMessage handled by " << GetNameOfEntity(pMiner->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(FOREGROUND_RED | FOREGROUND_INTENSITY);
		pMiner->GetFSM()->ChangeState(GoHomeAndSleepTilRested::Instance());
		return true;
	}
	return false;
}


SingWithBobby* SingWithBobby::Instance()
{
	static SingWithBobby instance;
	return &instance;
}

void SingWithBobby::Enter(Miner* pMiner)
{
	Dispatch->DispatchMessage(
		SEND_MSG_IMMEDIATELY,
		pMiner->ID(),
		ent_Bobby,
		Msg_SingWithMe,
		NO_ADDITIONAL_INFO
	);
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"This is our song !";
	
}

void SingWithBobby::Execute(Miner* pMiner)
{
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"singing...";
}

void SingWithBobby::Exit(Miner* pMiner)
{
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"Time to go back home, That was great !";
}

bool SingWithBobby::OnMessage(Miner* agent, const Telegram& msg)
{
	return false;
}


FightWithBobby* FightWithBobby::Instance()
{
	static FightWithBobby instance;
	return &instance;
}

void FightWithBobby::Enter(Miner* pMiner)
{
	// Bob starts a fight with Bobby in the saloon
	Dispatch->DispatchMessage(
		SEND_MSG_IMMEDIATELY,
		pMiner->ID(),
		ent_Bobby,
		Msg_FightWithMe,
		NO_ADDITIONAL_INFO
	);
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"This music drives me crazy!";
}

void FightWithBobby::Execute(Miner* pMiner)
{
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"FIGHT";

	pMiner->IncreaseFatigue();
	
	// If they get caught while fighting, they end up in drunktank 
	// until they are fully rested
	if (RandFloat() < GoToDrunkTankProbability)
	{
		pMiner->GetFSM()->ChangeState(SleepInDrunkTankM::Instance());
	}
	else
	{
		pMiner->GetFSM()->ChangeState(GoHomeAndSleepTilRested::Instance());
	}
}

void FightWithBobby::Exit(Miner* pMiner)
{
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"";
}

bool FightWithBobby::OnMessage(Miner* agent, const Telegram& msg)
{
	return false;
}


TalkWithBobby* TalkWithBobby::Instance()
{
	static TalkWithBobby instance;
	return &instance;
}

void TalkWithBobby::Enter(Miner* pMiner)
{
	SetTextColor(FOREGROUND_RED | FOREGROUND_INTENSITY);
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"Hey Bobby !";
	pMiner->SetMood(horny);
}

void TalkWithBobby::Execute(Miner* pMiner)
{
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"talking...";

	// there is 1 chance on 3 that Bob starts to interact 
	// with Bobby or Elsa 

	
	if (RandFloat() < 0.33)
	{
		switch (pMiner->Mood())
		{
		case happy:
			pMiner->GetFSM()->ChangeState(SingWithBobby::Instance());
			break;

		case horny:
			pMiner->GetFSM()->ChangeState(SendSextoToElsa::Instance());
			break;
		
		case angry:
			pMiner->GetFSM()->ChangeState(FightWithBobby::Instance());
			break;
		}
	}
}

void TalkWithBobby::Exit(Miner* pMiner)
{

}

bool TalkWithBobby::OnMessage(Miner* pMiner, const Telegram& msg)
{
	return false;
}



SleepInDrunkTankM* SleepInDrunkTankM::Instance()
{
	static SleepInDrunkTankM instance;
	return &instance;
}

void SleepInDrunkTankM::Enter(Miner* pMiner)
{
	if (pMiner->Location() != drunktank)
	{
		pMiner->ChangeLocation(drunktank);
	}
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"Gonna spend the worst night of my life, am I ?";
}

void SleepInDrunkTankM::Execute(Miner* pMiner)
{
	// Sleep in drunktank until Bob is rested
	pMiner->DecreaseFatigue();
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"zZz";

	if (!pMiner->Fatigued())
	{
		pMiner->GetFSM()->ChangeState(EnterMineAndDigForNugget::Instance());
	}
}

void SleepInDrunkTankM::Exit(Miner* pMiner)
{
	cout << "\n" << GetNameOfEntity(pMiner->ID()) << ": " <<
		"Glad to leave this cell, let's get back to work!";
}

bool SleepInDrunkTankM::OnMessage(Miner* agent, const Telegram& msg)
{
	return false;
}

