#include <fstream>
#include <time.h>

#include "Locations.h"
#include "Miner.h"
#include "MinersWife.h"
#include "EntityManager.h"
#include "MessageDispatcher.h"
#include "../../Common/misc/ConsoleUtils.h"
#include "EntityNames.h"
#include "Drunkard.h"
#include "Song.h"
#include "JukeBox.h"
#include <thread>
#include "MusicalGenre.h"




std::ofstream os;



int main()
{
//define this to send output to a text file (see locations.h)
#ifdef TEXTOUTPUT
  os.open("output.txt");
#endif
  InitJukeBox(); 
  thread first(StartJukeBox); // start on an another thread the music displayed in the saloon
  waveOutSetVolume(0, 0);
  //seed random number generator
  srand((unsigned) time(NULL));

  //create a miner
  Miner* Bob = new Miner(ent_Miner_Bob);

  //create his wife
  MinersWife* Elsa = new MinersWife(ent_Elsa);

  //create a drunkard
  Drunkard* Bobby = new Drunkard(ent_Bobby);

  //register them with the entity manager
  EntityMgr->RegisterEntity(Bob);
  EntityMgr->RegisterEntity(Elsa);
  EntityMgr->RegisterEntity(Bobby);
  //run Bob and Elsa through a few Update calls
  for (int i=0; i<30; ++i)
  { 
    Bob->Update();
    Elsa->Update();
	Bobby->Update();
	if (Bob->Location() == saloon || Bobby->Location() == saloon) { // check if someone can here the music if true put the volume on
		waveOutSetVolume(0, 0xFFFF);
	}
	else
	{
		waveOutSetVolume(0, 0);
	}
		
	//dispatch any delayed messages
    Dispatch->DispatchDelayedMessages();

    Sleep(1600);
  }

  //tidy up
  delete Bob;
  delete Elsa;
  delete Bobby;

  //wait for a keypress before exiting
  PressAnyKeyToContinue();
 

  return 0;
}






