#pragma once
#ifndef DRUNKARD_OWNED_STATES_H
#define DRUNKARD_OWNED_STATES_H
//------------------------------------------------------------------------
//
//  Name:   DrunkardOwnedStates.h
//
//  Desc:   All the states that can be assigned to the Drunkard class.
//          Note that a global state has not been implemented.
//
//  Author: Mat Buckland 2002 (fup@ai-junkie.com)
//
//------------------------------------------------------------------------
#include "../../Common/fsm/State.h"


class Drunkard;
struct Telegram;


//------------------------------------------------------------------------
//  In this state the drunkard will walk to a street and steal money from
//  from people's pockets. If he has enough money, he'll change state to 
//  GetDrunk. 
//------------------------------------------------------------------------
class EnterStreetAndDigInPockets : public State<Drunkard>
{
private:

	EnterStreetAndDigInPockets() {}

	//copy ctor and assignment should be private
	EnterStreetAndDigInPockets(const EnterStreetAndDigInPockets&);
	EnterStreetAndDigInPockets& operator=(const EnterStreetAndDigInPockets&);

public:

	//this is a singleton
	static EnterStreetAndDigInPockets* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* agent, const Telegram& msg);

};


//------------------------------------------------------------------------
//  drunkard changes location to the saloon and keeps buying Whiskey until
//  he is out of money. When he is out of money, he returns in the street 
//------------------------------------------------------------------------
class GetDrunk : public State<Drunkard>
{
private:

	GetDrunk() {}

	//copy ctor and assignment should be private
	GetDrunk(const GetDrunk&);
	GetDrunk& operator=(const GetDrunk&);

public:

	//this is a singleton
	static GetDrunk* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* agent, const Telegram& msg);
};



//------------------------------------------------------------------------
// Bob an Bobby are at the saloon at the same moment. 
// They talkt to each other
//------------------------------------------------------------------------
class TalkWithBob : public State<Drunkard>
{
private:

	TalkWithBob() {}

	//copy ctor and assignment should be private
	TalkWithBob(const TalkWithBob&);
	TalkWithBob& operator=(const TalkWithBob&);

public:

	//this is a singleton
	static TalkWithBob* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* agent, const Telegram& msg);
};

//------------------------------------------------------------------------
// Bob and Bobby got caught while fighting
// Bobby goes to jail
//------------------------------------------------------------------------
class SleepInPrison : public State<Drunkard>
{
private:

	SleepInPrison() {}

	//copy ctor and assignment should be private
	SleepInPrison(const SleepInPrison&);
	SleepInPrison& operator=(const SleepInPrison&);

public:

	//this is a singleton
	static SleepInPrison* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* agent, const Telegram& msg);
};

//------------------------------------------------------------------------
// If Bob sings, Bobby sings with him
//------------------------------------------------------------------------
class SingWithBob : public State<Drunkard>
{
private:

	SingWithBob() {}

	//copy ctor and assignment should be private
	SingWithBob(const SingWithBob&);
	SingWithBob& operator=(const SingWithBob&);

public:

	//this is a singleton
	static SingWithBob* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* agent, const Telegram& msg);
};

//------------------------------------------------------------------------
// If Bob fights, Bobby fights with him
//------------------------------------------------------------------------
class FightWithBob : public State<Drunkard>
{
private:

	FightWithBob() {}

	//copy ctor and assignment should be private
	FightWithBob(const FightWithBob&);
	FightWithBob& operator=(const FightWithBob&);

public:

	//this is a singleton
	static FightWithBob* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* agent, const Telegram& msg);
};

//------------------------------------------------------------------------
// If Bobby gets caught while fighting, he spends some time in jail
// Bobby goes back in the street after that
//------------------------------------------------------------------------
class SleepInDrunkTank : public State<Drunkard>
{
private:

	SleepInDrunkTank() {}

	//copy ctor and assignment should be private
	SleepInDrunkTank(const SleepInDrunkTank&);
	SleepInDrunkTank& operator=(const SleepInDrunkTank&);

public:

	//this is a singleton
	static SleepInDrunkTank* Instance();

	virtual void Enter(Drunkard* drunkard);

	virtual void Execute(Drunkard* drunkard);

	virtual void Exit(Drunkard* drunkard);

	virtual bool OnMessage(Drunkard* agent, const Telegram& msg);
};
#endif