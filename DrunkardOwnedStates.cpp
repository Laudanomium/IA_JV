﻿#include "DrunkardOwnedStates.h"

#include "../../Common/fsm/State.h"
#include "Drunkard.h"
#include "Locations.h"
#include "../../Common/messaging/Telegram.h"
#include "MessageDispatcher.h"
#include "MessageTypes.h"
#include "../../Common/Time/CrudeTimer.h"
#include "EntityNames.h"
#include "EntityManager.h"
#include "Miner.h"
#include <iostream>
using std::cout;


#ifdef TEXTOUTPUT
#include <fstream>
extern std::ofstream os;
#define cout os
#endif
#include "../../Common/misc/utils.h"


EnterStreetAndDigInPockets* EnterStreetAndDigInPockets::Instance()
{
	static EnterStreetAndDigInPockets instance;

	return &instance;
}

void EnterStreetAndDigInPockets::Enter(Drunkard* pDrunkard)
{
	if (pDrunkard->Location() != street)
	{
		cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": " 
			<< "Gonna rob some good ol' people in the street";

		pDrunkard->ChangeLocation(street);
	}
}

void EnterStreetAndDigInPockets::Execute(Drunkard* pDrunkard)
{


	// Now the drunkard is in the street, he digs in people's pockets
	// until he is carrying enough money to go to the saloon
	// to get drunk or until he gets caught and goes to jail
	if (RandFloat() < GetCaughtProbability)
	{
		cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": "
			<< "Oh no ! The cops !";
		pDrunkard->GetFSM()->ChangeState(SleepInPrison::Instance());
	}
	else
	{
		pDrunkard->AddToCashCarried(1);
		cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": "
			<< "Stealin' a coin. Total money : " << pDrunkard->CashCarried();
	}
	

	//if enough money in pockets, go to the saloon to get drunk
	if (pDrunkard->PocketsFull())
	{
		cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": "
			<< "Stealing all this money got me hella' thirsty";
		pDrunkard->GetFSM()->ChangeState(GetDrunk::Instance());
	}
}

void EnterStreetAndDigInPockets::Exit(Drunkard* pDrunkard)
{

}

bool EnterStreetAndDigInPockets::OnMessage(Drunkard* agent, const Telegram& msg)
{
	return false;
}


GetDrunk* GetDrunk::Instance()
{
	static GetDrunk instance;
	return &instance;
}

void GetDrunk::Enter(Drunkard* pDrunkard)
{	
	if (pDrunkard->Location() != saloon)
	{
		pDrunkard->ChangeLocation(saloon);
		waveOutSetVolume(0, 0xFFFF);
		cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": "
			<< "Feelin' thirsty already, going to the saloon is the answer";
	}
}

void GetDrunk::Execute(Drunkard* pDrunkard)
{
	// If the miner is in the saloon, Bobby alerts him of his arrival
	Miner* m = (Miner*)EntityMgr->GetEntityFromID(ent_Miner_Bob);
	if (m->Location() == saloon)
	{
		// Let Bob know that Bobby arrived to the saloon
		Dispatch->DispatchMessage(
			SEND_MSG_IMMEDIATELY,
			pDrunkard->ID(),
			ent_Miner_Bob,
			Msg_BobbyIsHere,
			NO_ADDITIONAL_INFO);
		pDrunkard->GetFSM()->ChangeState(TalkWithBob::Instance());
	}

	// If the drunkard is out of money, he goes back in the street
	if (pDrunkard->CashCarried() == 0)
	{
		cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": " 
			<< "My pockets are empty ?!";
		pDrunkard->GetFSM()->ChangeState(EnterStreetAndDigInPockets::Instance());
	}
	else
	{
		// Bobby drinks as long as he has money then goes back in the street
		pDrunkard->BuyAndDrinkAWhiskey();
		cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": "
			<< "Gettin' tipsy";
	}
}

void GetDrunk::Exit(Drunkard* pDrunkard)
{

}

bool GetDrunk::OnMessage(Drunkard* pDrunkard, const Telegram& msg)
{
	SetTextColor(BACKGROUND_BLUE | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);

	// If Bob comes in the saloon, Bobby talks with him instead
	// of going back to the street
	switch (msg.Msg)
	{
	case Msg_BobIsHere:

		cout << "\nMessage handled by " << GetNameOfEntity(pDrunkard->ID())
			<< " at time: " << Clock->GetCurrentTime();
		pDrunkard->GetFSM()->ChangeState(TalkWithBob::Instance());
		return true;
	}
	return false;
}



TalkWithBob* TalkWithBob::Instance()
{
	static TalkWithBob instance;

	return &instance;
}

void TalkWithBob::Enter(Drunkard* pDrunkard)
{
	SetTextColor(FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": "
		<< "What's up Bob ?";
}

void TalkWithBob::Execute(Drunkard* pDrunkard)
{
	cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": "
		"talking ...";
}

void TalkWithBob::Exit(Drunkard* pDrunkard)
{

}

bool TalkWithBob::OnMessage(Drunkard* pDrunkard, const Telegram& msg)
{
	SetTextColor(BACKGROUND_BLUE | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
	// Bobby waits for Bob to send him a message when 
	// they are talking
	// They can sing or fight together
	// If Bob leaves the saloon, the drunkard gets drunk
	switch (msg.Msg)
	{
	case Msg_FightWithMe:

		cout << "\nMessage handled by " << GetNameOfEntity(pDrunkard->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		pDrunkard->GetFSM()->ChangeState(FightWithBob::Instance());
		return true;

	case Msg_SingWithMe:

		cout << "\nMessage handled by " << GetNameOfEntity(pDrunkard->ID())
			<< " at time: " << Clock->GetCurrentTime();

		SetTextColor(FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		pDrunkard->GetFSM()->ChangeState(SingWithBob::Instance());
		return true;

	case Msg_LeavingSaloon:
		cout << "\nMessage handled by " << GetNameOfEntity(pDrunkard->ID())
			<< " at time: " << Clock->GetCurrentTime();
		SetTextColor(FOREGROUND_BLUE | FOREGROUND_INTENSITY);
		pDrunkard->GetFSM()->ChangeState(GetDrunk::Instance());
		return true;
	}
	return false;
}

SleepInPrison* SleepInPrison::Instance()
{
	static SleepInPrison instance;
	return &instance;
}

void SleepInPrison::Enter(Drunkard* pDrunkard)
{
	// The drunkard got caught while stealing in the street
	// he loses his money and sleeps in jail
	pDrunkard->SetCashCarried(0);
	cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": "
		"Gonna think about my wrongs in jail. Total money : " << pDrunkard->CashCarried();
	
}

void SleepInPrison::Execute(Drunkard* pDrunkard)
{
	
	cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": "
		"ZZZ";

	pDrunkard->GetFSM()->ChangeState(EnterStreetAndDigInPockets::Instance());
}

void SleepInPrison::Exit(Drunkard* pDrunkard)
{
	cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": "
		"I'll be a good citizen now that I'm out of prison, I swear.";
}

bool SleepInPrison::OnMessage(Drunkard* agent, const Telegram& msg)
{
	return false;
}

SingWithBob* SingWithBob::Instance()
{
	static SingWithBob instance;

	return &instance;
}

void SingWithBob::Enter(Drunkard* pDrunkard)
{

}

void SingWithBob::Execute(Drunkard* pDrunkard)
{
	cout << "\n" << GetNameOfEntity(pDrunkard->ID()) << ": " <<
		"singing...";
}

void SingWithBob::Exit(Drunkard* pDrunkard)
{

}

bool SingWithBob::OnMessage(Drunkard* agent, const Telegram& msg)
{
	return false;
}


FightWithBob* FightWithBob::Instance()
{
	static FightWithBob instance;
	return &instance;
}

void FightWithBob::Enter(Drunkard* pDrunkard)
{

}

void FightWithBob::Execute(Drunkard* pDrunkard)
{

}

void FightWithBob::Exit(Drunkard* pDrunkard)
{

}

bool FightWithBob::OnMessage(Drunkard* agent, const Telegram& msg)
{
	return false;
}




