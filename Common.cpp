#include "Common.h"

JukeBox joncquille; // create a global variable with the jukebox
void InitJukeBox()
{
	vector<Song> playList;
	playList.push_back(Song("Dominique.mp3", music_genre::folk, "0", "14000"));
	playList.push_back(Song("Celine.mp3", music_genre::romance, "60000", "74000"));
	playList.push_back(Song("FreeBird.mp3", music_genre::rock, "25000", "39000"));

	JukeBox juke(playList);
	joncquille = juke;	

}
void StartJukeBox() {
	joncquille.StartSong();
}