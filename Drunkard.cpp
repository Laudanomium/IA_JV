#include "Drunkard.h"

bool Drunkard::HandleMessage(const Telegram& msg)
{
	return d_pStateMachine->HandleMessage(msg);
}


void Drunkard::Update()
{
	SetTextColor(FOREGROUND_BLUE | FOREGROUND_INTENSITY);
	d_pStateMachine->Update();
}



void Drunkard::AddToCashCarried(const int val)
{
	d_iCashCarried += val;

	if (d_iCashCarried < 0) d_iCashCarried = 0;
}

/*
void Drunkard::AddToWealth(const int val)
{
	d_iMoneyInBank += val;

	if (d_iMoneyInBank < 0) d_iMoneyInBank = 0;
}

bool Drunkard::Thirsty()const
{
	if (d_iThirst >= ThirstLevel) { return true; }

	return false;
}


bool Drunkard::Fatigued()const
{
	if (d_iFatigue > TirednessThreshold)
	{
		return true;
	}

	return false;
}
*/
