#ifndef DRUNKARD_H
#define DRUNKARD_H
//------------------------------------------------------------------------
//
//  Name:   Drunkard.h
//
//  Desc:   A class defining a drunk Bobby.
//
//  Author: Mat Buckland 2002 (fup@ai-junkie.com)
//
//------------------------------------------------------------------------
#include <string>
#include <cassert>
#include <iostream>

#include "BaseGameEntity.h"
#include "Locations.h"
#include "../../Common/misc/ConsoleUtils.h"
#include "DrunkardOwnedStates.h"
#include "../../Common/fsm/StateMachine.h"

template <class entity_type> class State; //pre-fixed with "template <class entity_type> " for vs8 compatibility

struct Telegram;

//the amount of cash Bobby must have before going to the saloon
const int CashCapacity = 5;

//the chance to get caught while stealing 
const double GetCaughtProbability = 0.1;


class Drunkard : public BaseGameEntity
{
private:

	//an instance of the state machine class
	StateMachine<Drunkard>*  d_pStateMachine;

	//how much cash the drunkard has in his pockets
	int                   d_iCashCarried;

	//the drunkard got caught or not
	bool                   d_iGetCaught;

	//the drunkard state 
	int						d_iDrunkeness;

public:

	Drunkard(int id) :
		d_iCashCarried(0),
		d_iGetCaught(false),
		d_iDrunkeness(0),
		BaseGameEntity(id)

	{
		//set up state machine
		d_pStateMachine = new StateMachine<Drunkard>(this);

//		d_pStateMachine->SetCurrentState(GoHomeAndSleepTilRested::Instance());

		d_pStateMachine->SetCurrentState(EnterStreetAndDigInPockets::Instance());

		/* NOTE, A GLOBAL STATE HAS NOT BEEN IMPLEMENTED FOR THE MINER */
		
	}

	~Drunkard() { delete d_pStateMachine; }

	//this must be implemented
	void Update();

	//so must this
	virtual bool  HandleMessage(const Telegram& msg);


	StateMachine<Drunkard>* GetFSM()const { return d_pStateMachine; }



	//-------------------------------------------------------------accessors

	int           CashCarried()const { return d_iCashCarried; }
	void          SetCashCarried(int val) { d_iCashCarried = val; }
	void          AddToCashCarried(int val);
	bool          PocketsFull()const { return d_iCashCarried >= CashCapacity; }

	void		  IncreaseDrunkeness() { d_iDrunkeness += 1; };
	void		  DecreaseDrunkeness() { d_iDrunkeness -= 1; };

	void          BuyAndDrinkAWhiskey() { d_iCashCarried -= 1; d_iDrunkeness += 1; }
	
};

#endif

