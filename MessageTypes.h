#ifndef MESSAGE_TYPES
#define MESSAGE_TYPES

#include <string>

enum message_type
{
  Msg_HiHoneyImHome,
  Msg_StewReady,
  Msg_IWantYou,
  Msg_BobIsHere,
  Msg_BobbyIsHere,
  Msg_HiBob,
  Msg_HiBobby,
  Msg_SingWithMe,
  Msg_FightWithMe,
  Msg_ComeHome,
  Msg_DontComeHome,
  Msg_LeavingSaloon
};


inline std::string MsgToStr(int msg)
{
  switch (msg)
  {
  case Msg_HiHoneyImHome:
    
    return "HiHoneyImHome"; 

  case Msg_StewReady:
    
    return "StewReady";

  case Msg_IWantYou:
	  return "IWantYou";

  case Msg_BobIsHere:
	  return "BobIsHere";

  case Msg_BobbyIsHere:
	  return "BobbyIsHere";

  case Msg_FightWithMe:
	  return "FightWithMe";

  case Msg_SingWithMe:
	  return "SingWithMe";

  case Msg_ComeHome:
	  return "ComeHome";

  case Msg_DontComeHome:
	  return "DontComeHome";

  case Msg_HiBob:
	  return "HiBob";

  case Msg_HiBobby:
	  return "HiBobby";

  case Msg_LeavingSaloon:
	  return "LeavingSaloon";

  default:

    return "Not recognized!";
  }
}

#endif