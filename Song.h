#pragma once
#include <string>
#include "MusicalGenre.h"
using namespace std;

// Class who represents a song extract 
 class Song
{
private:
	string path;
	music_genre genre;

public:
	string start;
	string end;
	Song(string s, music_genre musicGenre, string start, string end) : path(s),genre(musicGenre),start(start),end(end){}
	music_genre GetMusicGenre() {
		return genre;
	}
	string GetPath() {
		return path;
	}
	virtual ~Song()
	{
	}

};

