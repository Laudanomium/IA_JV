#ifndef MUSICALGENRE_H
#define MUSICALGENRE_H


enum music_genre
{
	rock,
	folk,
	romance
};

#endif